#ifndef COMPUTER_H_INCLUDED
#define COMPUTER_H_INCLUDED

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include "numerique.h"
#include "entier.h"
#include "rationnel.h"
#include "reel.h"
#include "complexe.h"

#define PI 3.14

class ComputerException {
    char info[256];
public:
    ComputerException(const char* s) {std::strcpy(info,s); }
    const char* getInfo() const {return info;}
};

inline Rationnel* floatTOrat(std::string s)
{
    // supprimer les 0 de droite
    unsigned int i=s.size();
    while (s[i]==0 && i>0 && s[i]!='.') i--;
    s.erase(i+1, s.size()-i+1);
    i=0;
    while (s[i]!='.')
        i++;
    unsigned int j=1;
    i++;
    while (s[i]!='\0')
    {
        j*=10;
        i++;
    }
    double d= atof(s.c_str());
    d*=j;
    return new Rationnel(static_cast<int> (d),j);
}

inline bool estEntier(double nombre)
{
  return nombre == std::floor(nombre);
}


#endif // COMPUTER_H_INCLUDED
