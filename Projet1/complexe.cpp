#include "complexe.h"
#include "numerique.h"
#include "computer.h"

Complexe* Complexe::operator+(Complexe& c)
{
    Numerique* RE=(*reel)+(*c.reel);
    Numerique* IM=(*im)+(*c.im);
    return new Complexe(RE, IM);
}


Complexe* Complexe::operator+(Numerique& n)
{
    Numerique* RE= n+(*reel);
    return new Complexe(RE, im);
}

Complexe* Complexe::operator-(Complexe& c)
{
    Numerique* RE=(*reel)-(*c.reel);
    Numerique* IM=(*im)-(*c.im);
    return new Complexe(RE, IM);
}

Complexe* Complexe::operator-(Numerique& n)
{
    Numerique* RE= n-(*reel);
    return new Complexe(RE, im);
}

Complexe* Complexe::operator*(Complexe& c)
{
    Numerique* RE= *((*reel)*(*c.im))-*((*im)*(*c.reel));
    Numerique* IM= *((*reel) * (*c.im)) + *((*im) *(*c.reel));
    return new Complexe(RE, IM);
}

Complexe* Complexe::operator*(Numerique& n)
{
    Numerique* RE= (*reel)*n;
    Numerique* IM= (*reel)*n;
    return new Complexe(RE, IM);
}

//Complexe* Complexe::operator/(Complexe& c);
Complexe* Complexe::operator/(Numerique& n)
{
    Numerique* RE= (*reel)/n;
    Numerique* IM= (*im)/n;
    return new Complexe(RE, IM);
}

Complexe* Complexe::NEG()
{
    return new Complexe(reel->NEG(), im->NEG());
}

Complexe* Complexe::NUM() {throw ComputerException("Numerateur d'un complexe n'existe pas"); }
Complexe* Complexe::DEN() {throw ComputerException("Denominateur d'un complexe n'existe pas"); }

Numerique* Complexe::ARG()
{
    return NULL;
}

Numerique* Complexe::NORM()
{
    Entier deux(2);
    Numerique* res = ((*reel->POW(deux))+(*im->POW(deux)))->SQRT();
    Entier* entier= dynamic_cast<Entier*>(res);
    if(entier==0)
    {
        Reel* reel= dynamic_cast<Reel*>(res);
        if(reel==0)
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(res);
            return new Rationnel(*rationnel);
        }
        return new Reel(*reel);
    }
    return new Entier(*entier);
}


Numerique* Complexe::operator==(Complexe& n)
{
    if(n.reel==reel && n.im==im)
        return new Entier(1);
    return new Entier(0);
}
