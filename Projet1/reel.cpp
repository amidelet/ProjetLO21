#include "entier.h"
#include "reel.h"
#include "rationnel.h"
#include "computer.h"

Numerique* Reel::operator+(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            double division = (double) rationnel->getNumerateur()/rationnel->getDenominateur();
            return new Reel(d+division);
        }
        // cas o� c'est un r�el
        return new Reel(d+reel->d);
    }
    // cas o� c'est un entier
    return new Reel(d+static_cast<double>(entier->getValue()));
}

Numerique* Reel::operator-(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            double division = (double) rationnel->getNumerateur()/rationnel->getDenominateur();
            return new Reel(d-division);
        }
        // cas o� c'est un r�el
        return new Reel(d-reel->d);
    }
    // cas o� c'est un entier
    return new Reel(d-static_cast<double>(entier->getValue()));
}

Numerique* Reel::operator*(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            double division = (double) rationnel->getNumerateur()/rationnel->getDenominateur();
            return new Reel(d*division);
        }
        // cas o� c'est un r�el
        return new Reel(d*reel->d);
    }
    // cas o� c'est un entier
    return new Reel(d*static_cast<double>(entier->getValue()));
}

Numerique* Reel::operator/(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            if(rationnel->getNumerateur()==0)
                throw ComputerException("Denominateur �gal � 0");
            else
            {
                double division = (double) rationnel->getNumerateur()/rationnel->getDenominateur();
                return new Reel(d/division);
            }
        }
        // cas o� c'est un r�el
        if (reel->getValue()==0.)
            throw ComputerException("Denominateur �gal � 0");
        else
            return new Reel(d/reel->d);
    }
    // cas o� c'est un entier
    if (entier->getValue()==0)
        throw ComputerException("Denominateur �gal � 0");
    else
        return new Reel(d/entier->getValue());
}

Numerique* Reel::NEG() {return new Reel(-1.*d); }

Numerique* Reel::NUM() {throw ComputerException("Numerateur d'un reel n'existe pas"); }
Numerique* Reel::DEN() {throw ComputerException("Denominateur d'un reel n'existe pas"); }

Numerique* Reel::POW(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0)
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel==0)
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            double division= (double) rationnel->getNumerateur()/rationnel->getDenominateur();
            return new Reel(std::pow((double)d,division));
        }
        return new Reel(std::pow((double)d,reel->d));
    }
    return new Reel(std::pow(d, entier->getValue()));
}

Numerique* Reel::SIN() {return new Reel(std::sin(d)); }
Numerique* Reel::COS() {return new Reel(std::cos(d)); }
Numerique* Reel::TAN() {return new Reel(std::tan(d)); }
Numerique* Reel::ARCSIN() {return new Reel(std::asin(d)); }
Numerique* Reel::ARCOS() {return new Reel(std::acos(d)); }
Numerique* Reel::ARCTAN() {return new Reel(std::atan(d)); }

Numerique* Reel::SQRT() {return new Reel(std::sqrt(d)); } // est-ce que �a peut �tre un entier? no pienso

Numerique* Reel::EXP()
{
    double res= std::exp(d);
    if(estEntier(res)) return new Entier(res);
    return new Reel(res);
}
Numerique* Reel::LN()
{
    double res= std::log(d);
    if(estEntier(res)) return new Entier(res);
    return new Reel(res);
}

Numerique* Reel::NORM() {return new Entier(1); }
Numerique* Reel::ARG() {return new Reel(PI/2); }

Numerique* Reel::operator==(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && entier->getValue()==d)//c'est un entier egal
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && reel->d==d)//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if((double)rationnel->getNumerateur()/rationnel->getDenominateur()==d)return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Reel::operator!=(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && entier->getValue()==d)//c'est un entier pas egal
        return new Entier(0);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && reel->d==d)//c'est un reel pas egal
        {
            return new Entier(0);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if((double)rationnel->getNumerateur()/rationnel->getDenominateur()==d)
                return new Entier(0);//c'est un rationnel pasegal
        }
    }
    return new Entier(1);
}

Numerique* Reel::operator<=(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && entier->getValue()<=d)//c'est un entier egal
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && d<=reel->d)//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(d<=(double)rationnel->getNumerateur()/rationnel->getDenominateur())
                return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Reel::operator>=(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && entier->getValue()>=d)//c'est un entier egal
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && d>=reel->d)//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(d>=(double)rationnel->getNumerateur()/rationnel->getDenominateur())
                return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Reel::operator>(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && entier->getValue()>d)//c'est un entier egal
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && d>reel->d)//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(d>(double)rationnel->getNumerateur()/rationnel->getDenominateur())
                return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Reel::operator<(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && entier->getValue()<d)//c'est un entier egal
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && d<reel->d)//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(d<(double)rationnel->getNumerateur()/rationnel->getDenominateur())
                return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Reel::operator&&(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && d &&entier->getValue())
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && d && reel->d)//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(d && (double)rationnel->getNumerateur()/rationnel->getDenominateur())return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Reel::operator||(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && (d || entier->getValue()))
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && (d || reel->d))//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(d || ((double)rationnel->getNumerateur()/rationnel->getDenominateur()))return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Reel::operator!()
{
    return new Entier(!d);
}

void Reel::affiche(std::ostream& f)
{
    f << this->getValue();
}
