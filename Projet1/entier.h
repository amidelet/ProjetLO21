#ifndef ENTIER_H
#define ENTIER_H

#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

#include "numerique.h"

class Entier : public Numerique {
    int e;
public:
    Entier(int E=0): e(E) {} // la valeur par d�faut est 0
    // Entier(std::string s) {} � d�velopper

    int getValue() const {return e; }

    // surcharge des op�rateurs

    Numerique* operator+(Numerique& n);
    Numerique* operator-(Numerique& n);
    Numerique* operator*(Numerique& n);
    Numerique* operator/(Numerique& n);

    Numerique* DIV(Numerique& n);
    Numerique* operator%(Numerique& n);

    Numerique* NEG();

    Numerique* NUM();
    Numerique* DEN();

    Numerique* POW(Numerique& n);

    Numerique* RE() {return new Entier(e); }
    Numerique* IM() {return new Entier(); }

    Numerique* SIN();
    Numerique* COS();
    Numerique* TAN();
    Numerique* ARCSIN();
    Numerique* ARCOS();
    Numerique* ARCTAN();

    Numerique* SQRT();
    Numerique* EXP();
    Numerique* LN();

    Numerique* ARG();
    Numerique* NORM();

    // Op�rateurs logiques

    Numerique* operator==(Numerique& n);
    Numerique* operator!=(Numerique& n);

    Numerique* operator<=(Numerique& n);
    Numerique* operator>=(Numerique& n);
    Numerique* operator<(Numerique& n) ;
    Numerique* operator>(Numerique& n) ;

    Numerique* operator&&(Numerique& n);
    Numerique* operator||(Numerique& n);
    Numerique* operator!();

    void affiche(std::ostream& f = std::cout);

};

inline std::ostream& operator<<(std::ostream& F, Entier* entier)
{
    F << entier->getValue();
    return F;
}

#endif
