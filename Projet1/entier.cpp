#include "entier.h"
#include "reel.h"
#include "rationnel.h"
#include "computer.h"

Numerique* Entier::operator+(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            Rationnel res(e*rationnel->getDenominateur()+rationnel->getNumerateur(), rationnel->getDenominateur());
            if(res.getDenominateur()==1) return new Entier(res.getNumerateur());
            return new Rationnel(res.getNumerateur(), res.getDenominateur());
        }
        // cas o� c'est un r�el
        return new Reel(static_cast<double>(e)+reel->getValue());
    }
    // cas o� c'est un entier
    return new Entier(e+entier->e);
}

Numerique* Entier::operator-(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            Rationnel res(e*rationnel->getDenominateur()-rationnel->getNumerateur(), rationnel->getDenominateur());
            if(res.getDenominateur()==1) return new Entier(res.getNumerateur());
            return new Rationnel(res.getNumerateur(), res.getDenominateur());
        }
        // cas o� c'est un r�el
        return new Reel(static_cast<double>(e)-reel->getValue());
    }
    // cas o� c'est un entier
    return new Entier(e-entier->e);
}

Numerique* Entier::operator*(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            Rationnel res(e*rationnel->getNumerateur(), rationnel->getDenominateur());
            if(res.getDenominateur()==1) return new Entier(res.getNumerateur());
            return new Rationnel(res.getNumerateur(), res.getNumerateur());
        }
        // cas o� c'est un r�el
        double res=(double)e*reel->getValue();
        if(estEntier(res)) return new Entier(res);
        return new Reel(res);
    }
    // cas o� c'est un entier
    return new Entier(e*entier->e);
}

Numerique* Entier::operator/(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            if(rationnel->getNumerateur()==0)
                throw ComputerException("Denominateur �gal � 0");
            else
                return new Rationnel(e*rationnel->getDenominateur(), rationnel->getNumerateur());
        }
        // cas o� c'est un r�el
        if(reel->getValue()==0.)
            throw ComputerException("Denominateur �gal � 0");
        else
            return new Reel(e/reel->getValue());
    }
    // cas o� c'est un entier
    if(entier->getValue()==0)
        throw ComputerException("Denominateur �gal � 0");
    else
        if (e%entier->e == 0)
            return new Entier(e/entier->e);
        else
            return new Reel((double)e/entier->e);
}

Numerique* Entier::NEG() {return new Entier(-1*e); }

Numerique* Entier::NUM() {return new Entier(e); }
Numerique* Entier::DEN() {return new Entier(1); }

Numerique* Entier::DIV(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        throw ComputerException ("Vous devez rentrer un entier");
    }
    return new Entier(e/entier->e);
}

Numerique* Entier::operator%(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        throw ComputerException ("Vous devez rentrer un entier");
    }
    return new Entier(e%entier->e);
}

Numerique* Entier::POW(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0)
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel==0)
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            double division= (double) rationnel->getNumerateur()/rationnel->getDenominateur();
            if(estEntier(division)) return new Entier(std::pow(e, division));
            return new Reel(std::pow((double)e,division));
        }
        return new Reel(std::pow((double)e,reel->getValue()));
    }
    return new Entier(std::pow(e, entier->e));
}

Numerique* Entier::SIN() {return new Reel(std::sin((double)e)); }
Numerique* Entier::COS() {return new Reel(std::cos((double)e)); }
Numerique* Entier::TAN() {return new Reel(std::tan((double)e)); }
Numerique* Entier::ARCSIN() {return new Reel(std::asin((double)e)); }
Numerique* Entier::ARCOS() {return new Reel(std::acos((double)e)); }
Numerique* Entier::ARCTAN() {return new Reel(std::atan((double)e)); }

Numerique* Entier::SQRT()
{
    double res=std::sqrt(e);
    if(estEntier(res))
        return new Entier(res);
    return new Reel(res);
}

Numerique* Entier::EXP()
{
    double res=std::exp(e);
    if(estEntier(res))
        return new Entier(res);
    return new Reel(res);
}

Numerique* Entier::LN()
{
    double res=std::log(e);
    if(estEntier(res))
        return new Entier(res);
    return new Reel(res);
}

Numerique* Entier::NORM() {return new Entier(1); }
Numerique* Entier::ARG() {return new Reel(PI/2); }

Numerique* Entier::operator==(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && entier->e==e)//c'est un entier egal
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && entier->e==e)//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if((double)rationnel->getNumerateur()/rationnel->getDenominateur()==e)return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Entier::operator!=(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && entier->e!=e)
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && e!=reel->getValue())
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if((double)rationnel->getNumerateur()/rationnel->getDenominateur()!=e)return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Entier::operator<=(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && e<=entier->e)
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && e<=reel->getValue())//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(e<=(double)rationnel->getNumerateur()/rationnel->getDenominateur())return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Entier::operator>=(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && e>=entier->e)
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && e>=reel->getValue())//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(e>=(double)rationnel->getNumerateur()/rationnel->getDenominateur())return new Entier(1);//c'est un rationnel egal
        }
    }
    // Qu'est ce qu'on fait si ce n'est pas un entier
    return new Entier(0);
}

Numerique* Entier::operator<(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && e<entier->e)
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && e<reel->getValue())//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(e<(double)rationnel->getNumerateur()/rationnel->getDenominateur())return new Entier(1);//c'est un rationnel egal
        }
    }
    // Qu'est ce qu'on fait si ce n'est pas un entier
    return new Entier(0);
}

Numerique* Entier::operator>(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && e>entier->e)
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && e>reel->getValue())//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(e>(double)rationnel->getNumerateur()/rationnel->getDenominateur())return new Entier(1);//c'est un rationnel egal
        }
    }
    // Qu'est ce qu'on fait si ce n'est pas un entier
    return new Entier(0);
}

Numerique* Entier::operator&&(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && e && entier->e)
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && e && reel->getValue())//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(e && (double)rationnel->getNumerateur()/rationnel->getDenominateur())return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Entier::operator||(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && (e || entier->e))
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && (e || reel->getValue()))//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(e || ((double)rationnel->getNumerateur()/rationnel->getDenominateur()))return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}

Numerique* Entier::operator!()
{
    return new Entier(!e);
}

void Entier::affiche(std::ostream& f) {
    f << this->getValue();
}
