#ifndef COMPLEXE_H_INCLUDED
#define COMPLEXE_H_INCLUDED

#include <iostream>
#include <string>
#include <cstdlib>

#include "numerique.h"
#include "complexe.h"

class Complexe {
    Numerique* reel;
    Numerique* im;
public:
    Complexe(Numerique* r, Numerique* i): reel(r), im(i) {}

    Numerique* getReel() const {return reel; }
    Numerique* getIm() const {return im; }

    Complexe() {}

    Complexe* operator+(Complexe& c);
    Complexe* operator+(Numerique& n);

    Complexe* operator-(Complexe& c);
    Complexe* operator-(Numerique& n);

    Complexe* operator*(Complexe& c);
    Complexe* operator*(Numerique& n);

    Complexe* operator/(Complexe& c);
    Complexe* operator/(Numerique& n);

    Complexe* DIV(Complexe& c) {return NULL; }
    Complexe* DIV(Numerique& n) {return NULL; }
    Complexe* operator%(Complexe& c) {return NULL; }
    Complexe* operator%(Numerique& n) {return NULL; }

    //Complexe* POW(Numerique& n);

    Complexe* NEG();

    Complexe* NUM();
    Complexe* DEN();

    Numerique* RE() {return reel; }
    Numerique* IM() {return im; }

    Complexe* SIN();
    Complexe* COS();
    Complexe* TAN();
    Complexe* ARCSIN();
    Complexe* ARCOS();
    Complexe* ARCTAN();

    Complexe* SQRT();
    Complexe* EXP();
    Complexe* LN();

    Numerique* ARG();
    Numerique* NORM();

    // opérateurs logiques

    Numerique* operator==(Complexe& n);
    Numerique* operator!=(Numerique& n);

    Numerique* operator<=(Numerique& n);
    Numerique* operator>=(Numerique& n);
    Numerique* operator<(Numerique& n) ;
    Numerique* operator>(Numerique& n) ;

    Numerique* operator&&(Numerique& n);
    Numerique* operator||(Numerique& n);
    Numerique* operator!();
};

/*inline Complexe* $(Numerique* n1, Numerique* n2)
{
    return new Complexe(n1, n2);
}
*/

#endif // COMPLEXE_H_INCLUDED
