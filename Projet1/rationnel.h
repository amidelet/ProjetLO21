#ifndef RATIONNEL_H_INCLUDED
#define RATIONNEL_H_INCLUDED

#include <iostream>
#include <string>
#include <cstdlib>

#include "numerique.h"
#include "computer.h"

class Rationnel : public Numerique {
    Entier num;
    Entier den;
    void simplification();
public:
    // Constructeurs � partir d'un fractionnel, d'un int et d'un double

    Rationnel(Entier n=0, Entier d=1) {/*Entier zero(0); if (d==zero) throw "Erreur, denominateur nul";*/ num=Entier(n); den=Entier(d); simplification();}

    int getNumerateur() const {return num.getValue(); }
    int getDenominateur() const {return den.getValue(); }

    void setNumerateur(int N) {num=N; }
    void setDenominateur(int D) {den=D; }

    Numerique* operator+(Numerique& n);
    Numerique* operator-(Numerique& n);
    Numerique* operator*(Numerique& n);
    Numerique* operator/(Numerique& n);

    Numerique* NEG();

    Numerique* DIV(Numerique& n) {return NULL; } // ou renvoie d'une excpetion
    Numerique* operator%(Numerique& n) {return NULL; } // ou renvoie d'une exceptio

    Numerique* NUM();
    Numerique* DEN();

    Numerique* POW(Numerique& n);

    Numerique* RE() {return new Rationnel(num, den); }
    Numerique* IM() {return new Entier(); }

    Numerique* SIN();
    Numerique* COS();
    Numerique* TAN();
    Numerique* ARCSIN();
    Numerique* ARCOS();
    Numerique* ARCTAN();

    Numerique* SQRT();
    Numerique* EXP();
    Numerique* LN();

    Numerique* ARG();
    Numerique* NORM();

    // Op�rateurs logiques

    /*Numerique* operator==(Numerique& n);
    Numerique* operator!=(Numerique& n);

    Numerique* operator<=(Numerique& n);
    Numerique* operator>=(Numerique& n);
    Numerique* operator<(Numerique& n) ;
    Numerique* operator>(Numerique& n) ;*/

   /* Numerique* operator&&(Numerique& n);
    Numerique* operator||(Numerique& n);
    Numerique* operator!();*/

    void affiche(std::ostream& f = std::cout);
};

// Revoir la fonction simplification

inline void Rationnel::simplification()
{
    Entier zero(0);
    if (getNumerateur()==0)
        {
            den=1;
            return;
        }
    if (getDenominateur()==0)
        return;
    int a=getNumerateur(), b=getDenominateur();
    if (a<0) a=(-1)*a;
    if (b<0) b=(-1)*b;
    while(a!=b)
        {
            if (a>b)
                a=a-b;
            else
                b=b-a;
        }
    setNumerateur(getNumerateur()/a);
    setDenominateur(getDenominateur()/b);
    if (getDenominateur()<0)
        {
            setDenominateur(-1*getDenominateur());
            setNumerateur(-1*getNumerateur());
        }
}

inline std::ostream& operator<<(std::ostream& F, Rationnel* frac)
{
    F << frac->getNumerateur();
    if (frac->getDenominateur()!=1) F <<"/" << frac->getDenominateur();
    return F;
}


#endif // RATIONNEL_H_INCLUDED
