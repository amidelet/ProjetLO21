#ifndef REEL_H
#define REEL_H

#include <iostream>
#include <string>
#include <cstdlib>

#include "numerique.h"
#include "computer.h"

class Reel : public Numerique {
    double d;
public:
    Reel(double D=0): d(D) {}
    double getValue() const {return d;}

    // surcharge des opérateurs

    Numerique* operator+(Numerique& n);
    Numerique* operator-(Numerique& n);
    Numerique* operator*(Numerique& n);
    Numerique* operator/(Numerique& n);

    Numerique* NEG();

    Numerique* DIV(Numerique& n) {return NULL; }
    Numerique* operator%(Numerique& n) {return NULL; }

    Numerique* NUM();
    Numerique* DEN();

    Numerique* POW(Numerique& n);

    Numerique* RE() {return new Reel(d); }
    Numerique* IM() {return new Entier(); }

    Numerique* SIN();
    Numerique* COS();
    Numerique* TAN();
    Numerique* ARCSIN();
    Numerique* ARCOS();
    Numerique* ARCTAN();

    Numerique* SQRT();
    Numerique* EXP();
    Numerique* LN();

    Numerique* ARG();
    Numerique* NORM();

    void affiche(std::ostream& f = std::cout);


    // Opérateurs logiques

    Numerique* operator==(Numerique& n);
    Numerique* operator!=(Numerique& n);

    Numerique* operator<=(Numerique& n);
    Numerique* operator>=(Numerique& n);
    Numerique* operator<(Numerique& n) ;
    Numerique* operator>(Numerique& n) ;

    Numerique* operator&&(Numerique& n);
    Numerique* operator||(Numerique& n);
    Numerique* operator!();

};

inline std::ostream& operator<<(std::ostream& F, Reel* reel)
{
    F << reel->getValue();
    return F;
}

#endif
