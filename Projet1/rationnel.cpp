#include "entier.h"
#include "reel.h"
#include "rationnel.h"
#include "computer.h"

Numerique* Rationnel::operator+(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            return new Rationnel(getNumerateur()*rationnel->getDenominateur()+getDenominateur()*rationnel->getNumerateur(),
                                 rationnel->getDenominateur()*getDenominateur());
        }
        // cas o� c'est un r�el
        double division = (double) getNumerateur()/getDenominateur();
        return new Reel(division+reel->getValue());
    }
    // cas o� c'est un entier
    return new Rationnel(getNumerateur()+entier->getValue()*getDenominateur(), den);
}

Numerique* Rationnel::operator-(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            return new Rationnel(getNumerateur()*rationnel->getDenominateur()-getDenominateur()*rationnel->getNumerateur(),
                                 rationnel->getDenominateur()*getDenominateur());
        }
        // cas o� c'est un r�el
        double division = (double) getNumerateur()/getDenominateur();
        return new Reel(division-reel->getValue());
    }
    // cas o� c'est un entier
    return new Rationnel(getNumerateur()-entier->getValue()*getDenominateur(), den);
}

Numerique* Rationnel::operator*(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            return new Rationnel(getNumerateur()*rationnel->getNumerateur(),rationnel->getDenominateur()*getDenominateur());
        }
        // cas o� c'est un r�el
        double division = (double) getNumerateur()/getDenominateur();
        return new Reel(division*reel->getValue());
    }
    // cas o� c'est un entier
    return new Rationnel(getNumerateur()*entier->getValue(), den);
}

Numerique* Rationnel::operator/(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0) // ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if (reel==0) // ce n'est pas un r�el => c'est donc un rationnel
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            if(rationnel->getNumerateur()==0)
                throw ComputerException("Denominateur �gal � 0");
            else
                return new Rationnel(getNumerateur()*rationnel->getDenominateur(),rationnel->getNumerateur()*getDenominateur());
        }
        // cas o� c'est un r�el
        if (reel->getValue()==0.)
            throw ComputerException("Denominateur �gal � 0");
        else
        {
            double division = (double) getNumerateur()/getDenominateur();
            return new Reel(division/reel->getValue());
        }
    }
    // cas o� c'est un entier
    if(entier->getValue()==0)
        throw ComputerException("Denominateur �gal � 0");
    else
        return new Rationnel(getDenominateur()*entier->getValue(), getNumerateur());
}

Numerique* Rationnel::NEG() {return new Rationnel(-1*getNumerateur(), getDenominateur());}

Numerique* Rationnel::NUM() {return new Entier(getNumerateur());}
Numerique* Rationnel::DEN() {return new Entier(getDenominateur());}

Numerique* Rationnel::POW(Numerique& n)
{
    Entier* entier= dynamic_cast<Entier*>(&n);
    if(entier==0)
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel==0)
        {
            Rationnel* rationnel= dynamic_cast<Rationnel*>(&n);
            double division= (double) rationnel->getNumerateur()/rationnel->getDenominateur();
            return new Reel(std::pow((double)getNumerateur(),division)/std::pow((double)getDenominateur(),division));
        }
        return new Reel(std::pow(getNumerateur(),reel->getValue())/std::pow(getDenominateur(),reel->getValue()));
    }
    return new Rationnel(std::pow(getNumerateur(), entier->getValue()), std::pow(getDenominateur(), entier->getValue()));
}

Numerique* Rationnel::SIN() {return new Reel(std::sin((double)getNumerateur()/getDenominateur())); }
Numerique* Rationnel::COS() {return new Reel(std::cos((double)getNumerateur()/getDenominateur())); }
Numerique* Rationnel::TAN() {return new Reel(std::tan((double)getNumerateur()/getDenominateur())); }
Numerique* Rationnel::ARCSIN() {return new Reel(std::asin((double)getNumerateur()/getDenominateur())); }
Numerique* Rationnel::ARCOS() {return new Reel(std::acos((double)getNumerateur()/getDenominateur())); }
Numerique* Rationnel::ARCTAN() {return new Reel(std::atan((double)getNumerateur()/getDenominateur())); }

Numerique* Rationnel::SQRT() {return new Reel((double)std::sqrt(getNumerateur())/std::sqrt(getDenominateur())); }
Numerique* Rationnel::EXP()  {return new Reel(std::exp((double)getNumerateur()/getDenominateur())); }
Numerique* Rationnel::LN()   {return new Reel((double)std::log(getNumerateur())-std::log(getDenominateur())); }

Numerique* Rationnel::NORM() {return new Entier(1); }
Numerique* Rationnel::ARG() {return new Reel(PI/2); }

/*Numerique* Rationnel::operator==(Numerique& n)
{
    Entier* entier=dynamic_cast<Entier*>(&n);
    if(entier!=0 && entier->getValue()==(entier)(*num)/(*den))//c'est un entier egal
        return new Entier(1);
    else if(entier==0)//ce n'est pas un entier
    {
        Reel* reel= dynamic_cast<Reel*>(&n);
        if(reel!=0 && reel->d==(double)num/den)//c'est un reel egal
        {
            return new Entier(1);
        }
        else if(reel==0)//ce n'est ni un entier ni un reel donc rationnel
        {
            Rationnel* rationnel=dynamic_cast<Rationnel*>(&n);
            if(num==rationnel->getNumerateur() && den==rationnel->getDenominateur())return new Entier(1);//c'est un rationnel egal
        }
    }
    return new Entier(0);
}*/

void Rationnel::affiche(std::ostream& f)
{
    f << this->getNumerateur() << "/" << this->getDenominateur();
}
