#ifndef NUMERIQUE_H_INCLUDED
#define NUMERIQUE_H_INCLUDED

#include <iostream>
#include <string>
#include <cstdlib>

class Numerique {
public:
    Numerique() {}

    virtual Numerique* operator+(Numerique& n) =0;
    virtual Numerique* operator-(Numerique& n) =0;
    virtual Numerique* operator*(Numerique& n) =0;
    virtual Numerique* operator/(Numerique& n) =0;

    virtual Numerique* NEG() =0;

    virtual Numerique* NUM() =0;
    virtual Numerique* DEN() =0;

    virtual Numerique* DIV(Numerique& n) =0;
    virtual Numerique* operator%(Numerique& n) =0;

    virtual Numerique* POW(Numerique& n) =0;

    virtual Numerique* RE() =0;
    virtual Numerique* IM() =0;

    virtual Numerique* SIN() =0;
    virtual Numerique* COS() =0;
    virtual Numerique* TAN() =0;
    virtual Numerique* ARCSIN() =0;
    virtual Numerique* ARCOS() =0;
    virtual Numerique* ARCTAN() =0;

    virtual Numerique* SQRT() =0;
    virtual Numerique* EXP() =0;
    virtual Numerique* LN() =0;

    virtual Numerique* ARG() =0;
    virtual Numerique* NORM() =0;

    virtual ~Numerique() {}

    //virtual Numerique* operator==(Numerique& n)=0;
    //virtual Numerique* operator!=()=0;
    //virtual Numerique* operator=<()=0;
    //virtual Numerique* operator>=()=0;

    virtual void affiche(std::ostream& f = std::cout) =0;
};

inline std::ostream& operator<< (std::ostream& f, Numerique& n)
{
    n.affiche(f);
    return f;
}

#endif // NUMERIQUE_H_INCLUDED
