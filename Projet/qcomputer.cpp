#include "qcomputer.h"
#include "computer.h"
#include <QWidget>
#include <QLineEdit>
#include <QTextEdit>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QDebug>

void QComputer::getNextCommande(){
        pile->setMessage("");
        QString c=commande->text();
        //extraction de chq element de la ligne
        QTextStream stream(&c);
        QString com;
        do{
            stream>>com;
            //if (com!="")controleur(com);
        }while(com!="");
        commande->clear();//pr pas que ça reste un chiffre
  }
QComputer::QComputer(QWidget* parent):QWidget(parent){
    //créer les obj pointés par les attributs de la classe QComputer

    vuePile=new QTableWidget(pile->getNbItemsToAffiche(),1,this);//1 une seule colonne
    message=new QLineEdit(this);

    commande=new QLineEdit(this);

    couche=new QVBoxLayout(this);

    pile=new Pile();

    //getInstance ns donne l'objet qui est unique, pcq on avait utilisé le Singleton

    controleur=new Controleur(ExpressionManager::getInstance(),*pile);

    //Positionner les objets sur la fenetre

    couche->addWidget(message);

    couche->addWidget(vuePile);

    couche->addWidget(commande);

    setLayout(couche);

    //mettre un titre à la fenetre

    this->setWindowTitle(QString("UTComputer"));

    //Colorer la barre de message et la rendre non éditable

    message->setStyleSheet("background:darkblue;color:yellow");

    message->setReadOnly(true);

    //Donner la bne apparence à vuePile et non modifiable

    //couleur de fond

    vuePile->setStyleSheet("background:darkcyan;color:white");

    //couleur titre vertical de vuePile

    vuePile->verticalHeader()->setStyleSheet("color:black");

    //rendre la table non modifiable, on ne peut pas lancer de trigger, il ne prend plus le signal des actions

    vuePile->setEditTriggers(QAbstractItemView::NoEditTriggers);

    //rendre le header horizontal non visible

    vuePile->horizontalHeader()->setVisible(false);

    //ajuster automatiquement la largeur de la colonne à la largeur de la fenetre

    vuePile->horizontalHeader()->setStretchLastSection(true);

    //créer une liste de labels i pour les lignes

    QStringList numberList;
    for(unsigned int i=pile->getNbItemsToAffiche();i>0;i--){
        QString str=QString::number(i);
        str+=" :";
        numberList<<str;
        //création de l'item de chaque ligne initialisée avec la chaine vide
        vuePile->setItem(i-1,0,new QTableWidgetItem(""));
    }

    //affectation de la liste de label au header vertical
    vuePile->setVerticalHeaderLabels(numberList);

    //largeur fixe en fonction d'items affichés
    vuePile->setFixedHeight(pile->getNbItemsToAffiche()*vuePile->rowHeight(0)+2);

    //connection des slots
    QObject::connect(pile, SIGNAL(modificationEtat()),this,SLOT(refresh()));
    QObject::connect(commande,SIGNAL(returnPressed()),this,SLOT(getNextCommande()));

    //premier message

    pile->setMessage("Bienvenue!");

    //donner le focus à la barre de commande

    commande->setFocus(Qt::OtherFocusReason);


}
//définir les 2 slots
void QComputer::refresh(){
    message->setText(pile->getMessage());
    unsigned int nb=0;
    for(unsigned int i=0;i<pile->getNbItemsToAffiche();i++){
        vuePile->item(i,0)->setText("");
     }
    //mettre à jour
    for(Pile::iterator it=pile->begin();it!=pile->end()&& nb<pile->getNbItemsToAffiche();it.operator++()){
                        vuePile->item(pile->getNbItemsToAffiche()-1-nb,0)->setText((*it).toString());
    }
}
